Git-flow workflow
===

~~~

## Branch names
* master
* develop
* feature
* bugfix
* hotfix
* release

~~~

## Merging strategy

![branches](https://www.braintime.de/wp-content/uploads/2014/09/4-2-1-1-gitflow.png)

~~~

## Tutorial
For this, i will refer to
[this](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow)
excellent tutorial by atlassian.
