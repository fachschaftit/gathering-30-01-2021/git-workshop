Tips, tricks and best practices
===

~~~

## Commit messages
- please write useful messages
- messages should hint what the commit is doing
- title shouldn't be longer than 50 characters
- empty line between title and body
- you can link a commit to an issue ('#34' or 'GIT-3141')
- always continue the sentence
```
'If applied, this commit will...'
```

~~~

## Frequency
- different people say different things
- rather commit often
- commits can be 'squashed' later

~~~

## Use aliases #1
| alias | git command   |
|-------|---------------|
| gst   | status        |
| gl    | pull          |
| gp    | push          |
| gpf   | push  --force |
| gf    | fetch         |

~~~

## Use aliases #2
| alias | git command                            |
|-------|----------------------------------------|
| gd    | difftool --extcmd='nvim -d'            |
| glog  | log --oneline --decorate --graph       |
| gloga | log --oneline --decorate --graph --all |
| gc    | commit -v                              |

~~~

## Use .gitignores
- for excluding binary files
- for excluding 'secret' files
